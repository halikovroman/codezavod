<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
ini_set('display_errors', true);

class VoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $vote = $this->getVotes(false, 1);
        return view('index', compact('vote'));
    }

    public function vote(Request $request)
    {
        $voteID = $request->input('vote_id');
        $options = $request->input('options');
        $userID = Auth::id();
        // возможность переголосовать
        DB::delete('delete from results where user_id=? and vote_id=?', [$userID, $voteID]);
        foreach ($options as $option) {
            DB::insert('insert into results (user_id, vote_id, option_id) values (?, ?, ?)', [$userID, $voteID, $option]);
        }
        $response = [];
        $response['header'] = 'Спасибо за голосование!';
        
        $rows = DB::select('select * from options where vote_id=?', [$voteID]);
        $allOptions=[];
        foreach ($rows as $row) {
            $allOptions[$row->id]= [
                'option_name' => $row->text,
                'count' => 0
            ];
        }
        
        $rows = DB::select('select option_id, count(*) as count from results
            where vote_id=?
            group by option_id
            order by option_id', [$voteID]);
        foreach ($rows as $row) {
            $allOptions[$row->option_id]['count']=$row->count;
        }

        $response['results']= $allOptions;
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    public function admin()
    {
        // неправильный редирект, костыль :(
        if (!$this->checkAdmin()) {
            return redirect('');
        } // исправить
        $votes = $this->getVotes(true);
        return view('admin', compact('votes'));
    }

    public function addVote(Request $request)
    {
        // неправильный редирект, костыль :(
        if (!$this->checkAdmin()) {
            return redirect('');
        } // исправить
        $voteName = $request->input('voteName');
        $voteType = $request->input('voteType');
        $options = $request->input('options');
        
        DB::insert('insert into votes (name, type, active) values (?, ?, ?)', [$voteName, $voteType, 1]);
        $voteID = DB::getPdo()->lastInsertId();
        
        foreach ($options as $option) {
            DB::insert('insert into options (vote_id, text) values (?, ?)', [$voteID, $option]);
        }
        echo $voteID;
    }

    public function getVotes($admin=false, $count=null)
    {
        if ($admin) {
            $sql = 'select v.id as vote_id, v.name, v.`type`, v.active, o.id as option_id, o.text, t1.count
                from votes v inner join options o on v.id=o.vote_id
                left join 
                (select option_id, count(*) as count from results 
                group by option_id
                ) as t1 on o.id=t1.option_id';
        } else {
            if ($count!=null) {
                $sql = 'select t1.id as vote_id, t1.name, t1.type, o.id as option_id, o.text, t1.active, "" as count from
                    (select * from votes v where v.active=1 limit '.$count.') as t1
                    inner join options o on t1.id=o.vote_id';
            } else {
                $sql = 'select t1.id as vote_id, t1.name, t1.type, o.id as option_id, o.text, t1.active, "" as count from
                    (select * from votes v where v.active=1) as t1
                    inner join options o on t1.id=o.vote_id';
            }
        }

        $rows =  DB::select($sql);
        $votes=[];
          
        foreach ($rows as $row) {
            if (!array_key_exists($row->vote_id, $votes)) {
                $votes[$row->vote_id]=[
                    'vote_id' => $row->vote_id,
                    'name' => $row->name,
                    'type' => $row->type,
                    'active' => $row->active,
                    'options' => []
                ];
            }
            $votes[$row->vote_id]['options'][] = [
                'option_id' => $row->option_id,
                'option_text' => $row->text,
                'count' => $row->count ? $row->count: 0,
            ];
        }
        if (count($votes)==1) {
            reset($votes);
            return $votes[key($votes)];
        }
        return $votes;
    }

    public function checkAdmin()
    {
        //return redirect('/login');
        $user = \App\User::find(Auth::id());
        //var_dump($user->admin);exit;
        if ($user->admin=='1') {
            //var_dump('redirect!');exit;
            //return redirect('/login');
            return true;
        }
        return false;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
