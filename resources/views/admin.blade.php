@extends('template')

@section('resources')
<script src="/../../resources/assets/js/admin.js"></script>
<link rel="stylesheet" href="/../../resources/assets/css/admin.css">
@endsection

@section('content')
 <h1>Admin panel</h1>
 <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#add-vote-modal">
    Новое голосование
  </button>
    @foreach ($votes as $vote)
        <div class="card list-group-item" >
            <h4 class="card-title">{{ $vote['name'] }}</h4>
            <h5>
             @if ($vote['type']==1) 
                    <span class="label label-primary">Одиночный выбор</span>
                @else 
                    <span class="label label-info">Множественный выбор</span>
                @endif
                @if ($vote['active']==1) 
                    <span class="label label-success">Активно</span>
                @else 
                    <span class="label label-warning">Неактивно</span>
                @endif
            </h5>
            <ul class="list-group list-group-flush">
                @foreach ($vote['options'] as $option)
                    <li class="list-group-item">{{ $option['option_text'] }}<span class="badge">{{ $option['count'] }}</span></li>
                @endforeach
            </ul>
        </div>
    @endforeach
<!-- добавить голосование -->
<div class="modal fade" id="add-vote-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Добавить голосование</h4>
      </div>
      <div class="modal-body">
        <form id='form-add' action="/vote/add" method="POST">
            <div class="input-group">
                <textarea required class="form-control" name='name' cols=50 rows=5 type="text" placeholder='Vote name'></textarea>
                <h4>Выберите тип голосования:</h4>
                <select id='vote-type' class='form-control'>
                    <option value="1">Один вариант для выбора</option>
                    <option value="2">Несколько вариантов для выбора</option>
                </select><br>
                <h4>Выберите количество вариантов:</h4>
                <select id='option-count' class='form-control'>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                </select>
                <h4>Введите варианты голосования:</h4>
                <div id='options-container'>
                    <h5>Вариант 1</h5>
                    <input required class='form-control' type='text' name=option1>
                </div>
                <div class='alert alert-info'></div> 
            </div><br>
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button id='btn-add-vote' type="button" class="btn btn-primary">Добавить</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection