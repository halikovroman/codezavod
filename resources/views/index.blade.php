@extends('template')

@section('resources')
<script src="/../../resources/assets/js/index.js"></script>
<link rel="stylesheet" href="/../../resources/assets/css/index.css">
@endsection

@section('content')
    @if ($vote)
    <div class='form-container'>
        <form id='vote-form' method='post'>
                <input class="form-check-input" type="hidden" name="vote_id" value="{{ $vote['vote_id'] }}">
                <h4 class='vote_name'>{{ $vote['name'] }}</h4>
                    @foreach ($vote['options'] as $optionArr)
                    <div class="form-check">
                    <label class="form-check-label">
                        @if ($vote['type']==1)
                            <input class="form-check-input" type="radio" name="vote" value="{{ $optionArr['option_id'] }}"> {{ $optionArr['option_text'] }}
                        @else
                            <input class="form-check-input" type="checkbox" name="vote" value="{{ $optionArr['option_id'] }}"> {{ $optionArr['option_text'] }}
                        @endif
                    </label>
                    </div>
                    @endforeach
            <div class="form-group">
                <button class="btn btn-primary" disabled id='btn-vote' type="button">Сохранить</button>
            </div>
        </form>
        <div class='alert alert-info'></div>     
    </div>
    @endif
@endsection 