<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Голосования</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/../../resources/assets/js/template.js"></script>
    @yield('resources')    
</head>
<body>
<!-- header -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    
    <ul class="nav navbar-nav">
        <li {{{ (Request::is('/') ? 'class=active' : '') }}}>
            <a href="{{action('VoteController@index')}}">Home</a>
        </li>
        <li {{{ (Request::is('admin') ? 'class=active' : '') }}}>
            <a href="{{action('VoteController@admin')}}">Admin panel</a>
        </li>
        @auth
            <li ><a href="{{ route('logout') }}">Logout</a></li>
        @else
            <li {{{ (Request::is('login') ? 'class=active' : '') }}}><a href="{{ route('login') }}">Login</a></li>
            <li {{{ (Request::is('register') ? 'class=active' : '') }}}><a href="{{ route('register') }}">Register</a></li>
        @endauth
    </ul>
  </div>
</nav>
    
<!-- /header -->
    <div class='content'>
        @yield('content')
    </div>
</body>
</html>