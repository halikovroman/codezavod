$(document).ready(function(){
    
    $('input[name=vote]').change(function(){
        var disable = true;
        $('input[name=vote]').each(function() {
            if ($(this).prop('checked')) {
                disable = false;
            }
        });
        $("#btn-vote").prop("disabled", disable);
    });

    $('#btn-vote').click(function(){
        $('.alert').hide();
        var request = {};
            request.vote_id = $('input[name=vote_id]').val();
            request.options = []
        
        $('input[name=vote]').each(function() {
            if ($(this).prop('checked')) {
                request.options.push($(this).val());
            }
        });

        $.post('/public/vote', request, function(response){
            $('.alert').empty();
            response = JSON.parse(response);
            //console.log(response);
            var h4 = $('<h4>'+response['header']+'</h4>')
            var h5 = $('<h5>Статистика голосов:</h5>')
            $('.alert').append(h4, h5);
            for (var option_id in response.results) {
                var optionArr = response.results[option_id];
                //console.log(optionArr)
                var p = $('<p>'+optionArr.option_name+': '+optionArr.count+'</p>');
                $('.alert').append(p);
            };
            $('.alert').show();
        });
    });
});