$(document).ready(function(){
    var voteType = $('#vote-type').val()

    $('select#vote-type').change(function(){
        voteType = $('#vote-type').val()
    });

    $('select#option-count').change(function(){
        $('div#options-container').empty()
        for(var i=0; i<$(this).val(); i++) {
            var input = $('<input required class=form-control type=text name=option'+(i+1)+'>');
            var header5 = $('<h5>Вариант '+(i+1)+'</h5>')
            $('div#options-container').append(header5)
            $('div#options-container').append(input)
        }
    })

    $('#btn-add-vote').click(function(){
        $('div.alert').hide();
        var voteName = $('textarea[name=name]').val().trim()
        if (!voteName) {
            $('div.alert').html('Не введено название голосования')
            $('div.alert').show();
            return;
        }
        var options = [];

        $('div#options-container').find('input').each(function(){
            options.push($(this).val().trim());
        });

        for (var i=0; i<options.length; i++) {
            if (options[i]=='') {
                $('div.alert').html('Не введено значение для варианта '+(i+1))
                $('div.alert').show();
                return;
            }
        }
        var request = {}
            request.voteName = voteName
            request.voteType = voteType
            request.options = options

        $.post('vote/add', request, function(response){
            $('div.alert').html(response);
            $('div.alert').show();
        })

    });
});