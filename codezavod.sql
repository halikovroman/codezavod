-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.13 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных codezavod
CREATE DATABASE IF NOT EXISTS `codezavod` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `codezavod`;


-- Дамп структуры для таблица codezavod.options
CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL DEFAULT '0',
  `text` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы codezavod.options: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` (`id`, `vote_id`, `text`) VALUES
	(1, 1, 'первый вариант'),
	(2, 1, 'второй вариант'),
	(3, 1, 'третий'),
	(4, 2, 'первый вар'),
	(5, 2, 'второй вар'),
	(6, 5, 'перв'),
	(7, 5, 'втор'),
	(8, 6, 'персик'),
	(9, 6, 'яблоко'),
	(10, 6, 'абрикос');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;


-- Дамп структуры для таблица codezavod.results
CREATE TABLE IF NOT EXISTS `results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `vote_id` int(11) NOT NULL DEFAULT '0',
  `option_id` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы codezavod.results: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
INSERT INTO `results` (`id`, `user_id`, `vote_id`, `option_id`, `date`) VALUES
	(42, 2, 1, 2, '2017-08-17 18:53:10'),
	(108, 1, 1, 1, '2017-08-17 19:17:55');
/*!40000 ALTER TABLE `results` ENABLE KEYS */;


-- Дамп структуры для таблица codezavod.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) NOT NULL DEFAULT '0',
  `admin` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы codezavod.users: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `admin`) VALUES
	(1, 'roman', 'halikovroman@mail.ru', '$2y$10$QLtJcJeEvwswbeGsLuezNOU1UIGzKjZKQjguxU.nkXLLxKqdxd4/a', 'zWgJQCV0pROcnJQGg4xfqwFfjP5nmswCW9bMdsXC9dAvQBEUaAJikAYsBaHZ', '1'),
	(2, 'azamat', 'azamat@mail.ru', '$2y$10$kvJDphWv5hGR5WOtCvIdZOjpQyuuKfcdgKvZ3PXlcLzc697yRrI/e', '2LjAGWkOCtDvLzzlSuv4QtFiwVgOhLs6BJ1PrxoKGcwIlz8avkNQHhc1JOqI', '0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Дамп структуры для таблица codezavod.votes
CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '1',
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы codezavod.votes: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `votes` DISABLE KEYS */;
INSERT INTO `votes` (`id`, `name`, `type`, `active`) VALUES
	(1, 'Первое голосование', 2, 1),
	(2, 'Второе голосование', 1, 1),
	(5, 'Третье голосование', 1, 1),
	(6, 'Фрукты', 2, 1);
/*!40000 ALTER TABLE `votes` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
